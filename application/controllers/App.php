<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('EmailModel');
    }

    public function testmail() {
        $config = array(
            'useragent' => 'nutmor.com',
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.postmarkapp.com',
            'smtp_port' => 587,
            'smtp_user' => 'e4d0462d-b4ff-433b-9f87-fdf266d57c2f',
            'smtp_pass' => 'e4d0462d-b4ff-433b-9f87-fdf266d57c2f',
            'smtp_crypto' => 'TLS',
            'mailtype' => 'html',
            'charset' => 'utf-8',
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('Partchayanan.y@softubon.com', "ผู้ดูแลระบบ");
        $this->email->to('partchayanan.y@gmail.com');
        $this->email->subject("Email Verification");
        $this->email->message("แจ้งยืนยันการสมัครสมาชิกถึงคุณ สมัครสมาชิกเรียบร้อยแล้ว ขอบคุณ");
        $this->email->send();
    }

    public function index() {
        $this->load->view('login');
    }

    public function hosting() {
        $this->load->view('header');
        $this->load->view('hosting');
        $this->load->view('footer');
    }
    
    public function ptot() {
        $this->load->view('header');
        $this->load->view('form/form_ptot');
        $this->load->view('footer');
    }

    public function confirm() {
        $this->load->view('confirm');
    }

    public function signup() {
        $this->load->view('signup');
    }

    public function monitor() {
        $this->load->view('header');
        $this->load->view('monitor');
        $this->load->view('footer');
    }

    public function dashboard() {
        $this->load->view('header');
        $this->load->view('dashboard');
        $this->load->view('footer');
    }

    public function chart_morris() {
        $this->load->view('header');
        $this->load->view('chart_morris');
        $this->load->view('footer');
    }

    public function create_account() {
        $keytime = md5(time());
        $data = array(
            "EMAIL" => $this->input->post('Username'),
            "PASSWORD" => md5($this->input->post('Password')),
            "StudentID" => $this->input->post('StudentID'),
            "StudentName" => $this->input->post('StudentName'),
            "Department" => $this->input->post('Department'),
            "email_verification_code" => $keytime
        );
        $idcard = $this->input->post('StudentID');
        $email = $this->input->post('Username');
        $query = $this->db->query("SELECT StudentID,EMAIL FROM tbstudent WHERE StudentID='$idcard' OR EMAIL='$email'");
        if ($query->num_rows() > 0) {
            $message['message'] = 'หมายเลขบัตรประชาหรืออีเมล์นี้มีในระบบแล้ว';
        } else {
            $email = $this->input->post('Username');
            $member = $this->input->post('StudentName');
            $this->EmailModel->sendVerificatinEmail($email, $member, $keytime);
            $this->db->insert("tbstudent", $data);
            redirect(base_url());
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url());
    }

    public function login() {
        $username = $this->input->post('Username');
        $password = md5($this->input->post('Password'));
        $query = $this->db->query("SELECT * FROM tbstudent WHERE EMAIL='$username' AND PASSWORD='$password'");
        $row = $query->row();
        if ($query->num_rows() > 0) {
            $this->session->set_userdata('username', $row->StudentID);
            $this->session->set_userdata('level', $row->Level);
            if ($row->Level == 0) {
                redirect(base_url('app/dashboard'));
            } else {
                redirect(base_url('app/monitor'));
            }
        } else {
            redirect(base_url());
        }
    }
    
    public function checklogin()
    {
        $username = $this->input->post('USERNAME');
        $password = md5($this->input->post('PASSWORD'));

        $this->db->select('*');
        $this->db->from('tbuser');
        $this->db->join('tbclinic', 'tbclinic.CLINICID = tbuser.CLINICID');
        $this->db->where('tbuser.UserID', $username);
        $this->db->where('tbuser.Password', $password);
        $query = $this->db->get();

        $row = $query->row();
        if ($query->num_rows() > 0) {
            if ($row->ACTIVATE == 1) {
                if ($row->Status == 1) {
                    $this->session->set_userdata('CLINICID', $row->CLINICID);
                    $this->session->set_userdata('UserID', $row->UserID);
                    $this->session->set_userdata('Level', $row->Leval);
                    redirect(base_url('app/dashboard'));
                } else {
                    $this->session->set_userdata('danger', 'ถูกระงับการใช้งานโปรดติดต่อผู้ดูแล');
                    redirect(base_url(''));
                }
            } else if ($row->ACTIVATE == 0) {
                redirect(base_url('app/waiteactivateaccount'));
            }
        } else {
            $this->session->set_userdata('danger', 'ไม่สามารถติดต่อฐานข้อมูลได้');
            redirect(base_url(''));
        }
    }

}
