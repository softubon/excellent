<?php

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('HomeModel');
        $this->load->model('EmailModel');
    }

    public function index() {
        $this->load->library('email');
        $this->load->library('postmark');
        $this->postmark->from('no-reply@nutmor.com', 'ทีมนัดหมอ');
        $this->postmark->to('partchayanan.y@softubon.com', 'Softubon.com');
        $this->postmark->subject('Example Postmark');
        $this->postmark->message('Testing...');
        $this->postmark->send();
    }

    public function verify($verificationText = NULL) {
        $noRecords = $this->HomeModel->verifyEmailAddress($verificationText);
        if ($noRecords > 0) {
            $error = "Email Verified Successfully!";
        } else {
            $error = "Sorry Unable to Verify Your Email!";
        }
        redirect(base_url('app/confirm'));
    }

    public function sendVerificationEmail() {
        $email = $this->input->post('EMAIL');
        $query = $this->db->query("select IDCARD,EMAIL from tbmembers where email='$email'");
        $row = $query->row();
        $emailmember = $row->EMAIL;
        $idcard = $row->IDCARD;
        $this->EmailModel->sendVerificatinEmail($emailmember, $idcard);
        $this->load->view('header');
        $this->load->view('frontpage');
        $this->load->view('footer');
    }

    public function resetpassword() {
        $email = $this->input->post('EMAIL');
        $query = $this->db->query("select CUSTOMERNAME,IDCARD,EMAIL from tbmembers where email='$email'");
        $row = $query->row();
        if ($query->num_rows() == 0) {
            $forgetpassword['forgetpassword'] = 'ไม่พบข้อมูลอีเมล์นี้ในระบบสมาชิก';
            $this->load->view('header');
            $this->load->view('signin', $forgetpassword);
            $this->load->view('footer');
        } else {
            $row = $query->row();
            $emailmember = $row->EMAIL;
            $idcard = $row->IDCARD;
            $membername = $row->CUSTOMERNAME;
            $digits = 6;
            $repassword = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
            $data = array(
                'verificationcode' => $repassword
            );
            $this->db->where('IDCARD', $idcard);
            $this->db->update('tbmembers', $data);
            $this->EmailModel->sendNewForgetPassword($emailmember, $membername, $repassword);
        }
    }

    public function sendforgetEmail() {
        $youcode = $this->input->post('youcode');
        $query = $this->db->query("select verificationcode,EMAIL from tbmembers where verificationcode='$youcode'");
        if ($query->num_rows() == 0) {
            $forgetpassword['forgetpassword'] = 'Verification Code ไม่ถูกต้อง';
            $this->load->view('header');
            $this->load->view('resetpassword', $forgetpassword);
            $this->load->view('footer');
        } else {
            $email['email'] = $this->input->post('EMAIL');
            $this->load->view('header');
            $this->load->view('resetemailpassword', $email);
            $this->load->view('footer');
        }
    }

    public function updatepassword() {
        $email = $this->input->post('EMAIL');
        $query = $this->db->query("select IDCARD,EMAIL from tbmembers where EMAIL='$email'");
        $row = $query->row();
        if ($query->num_rows() == 0) {
            $forgetpassword['forgetpassword'] = 'ไม่พบข้อมูลอีเมล์นี้ในระบบสมาชิก';
            $this->load->view('header');
            $this->load->view('signin', $forgetpassword);
            $this->load->view('footer');
        } else {
            $row = $query->row();
            $idcard = $row->IDCARD;
            $repassword = $this->input->post('PASSWORD');
            $data = array(
                'PASSWORD' => md5($repassword)
            );
            $this->db->where('IDCARD', $idcard);
            $this->db->update('tbmembers', $data);
            $forgetpassword['forgetpassword'] = 'อัปเดตรหัสผ่านใหม่แล้ว';
            $this->load->view('header');
            $this->load->view('signin', $forgetpassword);
            $this->load->view('footer');
        }
    }

}
