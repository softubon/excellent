<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailModel
 *
 * @author Partc
 */
class EmailModel extends CI_Model {

    public function __construct() {
        parent::__construct();  
    }

    public function sendVerificatinEmail($email, $member, $verificationText) {
        $config = array(
            'useragent' => 'ictutc.com',
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.postmarkapp.com',
            'smtp_port' => 587,
            'smtp_user' => 'e4d0462d-b4ff-433b-9f87-fdf266d57c2f',
            'smtp_pass' => 'e4d0462d-b4ff-433b-9f87-fdf266d57c2f',
            'smtp_crypto' => 'TLS',
            'mailtype' => 'html',
            'charset' => 'utf-8',
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('Partchayanan.y@softubon.com', "แผนกเทคโนโลยีสารสนเทศ วิทยาลัยเทคนิคอุบลราชธานี");
        $this->email->to($email);
        $this->email->subject("ยืนยันอีเมล์สมาชิก");
        $this->email->message("แจ้งยืนยันการสมัครสมาชิกถึงคุณ " . $member . "  กรุณาคลิ๊กที่ลิงค์ เพื่อทำการยืนยันที่อยู่บัญชีอีเมล์ของท่าน https://ictutc.com/register/home/verify/" . $verificationText . " ขอบคุณ");
        $this->email->send();
    }

    public function sendNewForgetPassword($emailmember, $membername, $repassword) {
        
        $config = array(
            'useragent' => 'ictutc.com',
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.postmarkapp.com',
            'smtp_port' => 587,
            'smtp_user' => 'e4d0462d-b4ff-433b-9f87-fdf266d57c2f',
            'smtp_pass' => 'e4d0462d-b4ff-433b-9f87-fdf266d57c2f',
            'smtp_crypto' => 'TLS',
            'mailtype' => 'html',
            'charset' => 'utf-8',
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('Partchayanan.y@softubon.com', "ทีมนัดหมอ");
        $this->email->to($emailmember);
        $this->email->subject("Nutmor Verification Code");
        $email_body = "<h1><b>$repassword</b></h1> รหัส 6 หลักด้านบนคือ Verification Code โปรดกรอกรหัสดังกล่าวเพื่อยืนยันการเปลี่ยนแปลงรหัส่านของคุณ ขอแสดงความนับถือ <b>ทีมพัฒนาระบบนัดหมอ</b>";
        $this->email->message($email_body);
        $this->email->send();
        $data['mail'] = $emailmember;
        $this->load->view('header');
        $this->load->view('resetpassword', $data);
        $this->load->view('footer');
    }

    public function sendUpdatePassword($emailmember, $membername, $repassword) {
        $config = array(
            'useragent' => 'ictutc.com',
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.postmarkapp.com',
            'smtp_port' => 587,
            'smtp_user' => 'e4d0462d-b4ff-433b-9f87-fdf266d57c2f',
            'smtp_pass' => 'e4d0462d-b4ff-433b-9f87-fdf266d57c2f',
            'smtp_crypto' => 'TLS',
            'mailtype' => 'html',
            'charset' => 'utf-8',
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('Partchayanan.y@softubon.com', "ทีมนัดหมอ");
        $this->email->to($emailmember);
        $this->email->subject("Nutmor Verification Code");
        $this->email->message("แจ้งเปลี่ยนรหัสสมาชิกถึงคุณ " . $membername . " ตามที่ท่านร้องขอการเปลี่ยนแปลงรหัสผ่านที่อยู่บัญชีอีเมล์ของท่าน การกระทำดังกล่าวได้สำเร็จแล้ว \n\nขอบคุณ\nทีมพัฒนาระบบนัดหมอ");
        $this->email->send();
        $forgetpassword['forgetpassword'] = 'อัปเดตรหัสผ่านใหม่แล้ว';
        $this->load->view('header');
        $this->load->view('signin', $forgetpassword);
        $this->load->view('footer');
    }

}
