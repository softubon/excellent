<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card planned_task">
            <div class="header">
                <h2><i class="fa fa-plus-circle"></i> New</h2>
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="col-6">
                    <form action="#" method="POST">
                        
                        <table border="3" class="table table-bordered" bordercolor="#006633" align="center">
                            <tr>
                                <td bgcolor="#CCFFCC">
                                    <b>&nbsp;Hand grip&nbsp;
                                        <input type="number" name="p_Rt" require>
                                        &nbsp;Kg.&nbsp;
                                        </td>
                                        <td bgcolor="#CCFFCC"><b>&nbsp;Gait speed<input type="number" name="p_Rt" require>
                                                <b>sec.</b>&nbsp;<input type="number" name="p_Rt" require>&nbsp;m/s
                                                </td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#CCFFCC"><b>&nbsp;TUGH&nbsp;</b><input type="number" name="p_Rt" require>
                                                        &nbsp;<b>sec.</b>&nbsp;
                                                    </td>
                                                    <td bgcolor="#CCFFCC"><b>&nbsp;GUG&nbsp;</b><select id='PPGU'>
                                                            <option value=0>0. ยืนได้โดยไม่ต้องใช้มือยัน</option>
                                                            <option value=1>1. ใช้มือยัน 1 ครั้งแล้วลุกเองได้</option>
                                                            <option value=3>3. พยายามใช้มือยันหลายครั้งจึงลุกได้</option>
                                                            <option value=4>4. ลุกยืนไม่ได้ ถ้าไม่มีคนช่วย</option>
                                                        </select></b>
                                        </td>
                            </tr>
                            <tr>
                                <td bgcolor="#CCFFCC"><b>&nbsp;BAI SIM&nbsp;<input type="number" name="p_Rt" require>
                                        
                                        </td>
                                        <td bgcolor="#CCFFCC">&nbsp;FAT%&nbsp; <input type="number" name="p_Rt" require>
                                            <b>%. VisFat</b>&nbsp;<input type="number" name="p_Rt" require>
                                        </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>