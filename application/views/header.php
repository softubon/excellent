<!doctype html>
<html lang="en">

    <head>
        <title>Excellent by Chevanon Clinic</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Electronic Patient Record 2021 | ระบบบริหารงานคลินิก">
        <meta name="author" content="Pratchayanan Yanphoem, design by: softubon.com">
        <link rel="icon" href="favicon.ico" type="image/x-icon">        <!-- VENDOR CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/animate-css/vivify.min.css">        <!-- MAIN CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/site.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/sweetalert/sweetalert.css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/chartist/css/chartist.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link href="https://fonts.googleapis.com/css2?family=Sarabun:wght@100&display=swap" rel="stylesheet">
    </head>
    <body class="theme-blue" style="font-family: 'Sarabun', sans-serif;">


        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>

        <div id="wrapper">

            <nav class="navbar navbar-fixed-top bg-blue">
                <div class="container-fluid">

                    <div class="navbar-left">
                        <div class="navbar-brand">
                            <a class="small_menu_btn" href="javascript:void(0);"><i class="fa fa-align-left"></i></a>
                            <a href="<?php echo base_url('app/dashboard') ?>"><span><b>โปรแกรมประเมินผู้สูงวัยสุขภาพดี ชีวานนท์ สหคลินิก</b></span></a> 
                        </div>
                       
                    </div>

                    <div class="navbar-right">
                        <div id="navbar-menu">
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown"><i class="fa fa-user"></i>
                                        <span class="notification-dot bg-light"></span> 
                                    </a>
                                    <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown"><i class="fa fa-envelope"></i>
                                        <span class="notification-dot bg-green"></span>
                                    </a>
                                    <ul class="dropdown-menu right_chat email vivify fadeIn">
                                        <li class="header">You have 1 New eMail</li>                                
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div class="media">
                                                    <div class="avtar-pic w35 lbg-indigo"><span>FC</span></div>
                                                    <div class="media-body">
                                                        <span class="name">สวัสดีสมาชิกทุกท่าน <small class="float-right">1min ago</small></span>
                                                        <span class="message">ยินดีต้อนรับเข้าสู่ระบบ</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>

                                    </ul>
                                </li>

                                </li>
                                <li><a href="<?php echo base_url('app/logout') ?>" class="icon-menu"><i class="fa fa-power-off"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>



            <div id="left-sidebar" class="sidebar">
                <div class="sidebar_icon">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link" href=""><i class="fa fa-search"></i></a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Home-icon"><i class="fa fa-dashboard"></i></a></li>    
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#Profile-icon"><i class="fa fa-user"></i></a>
                            <a class="nav-link" data-toggle="tab" href="#Setting-icon"><i class="fa fa-cog"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="sidebar_list">
                    <div class="tab-content" id="main-menu">
                        <div class="tab-pane active" id="Home-icon">
                            <nav class="sidebar-nav sidebar-scroll">
                                <ul class="metismenu">
                                    <li>
                                        <a href="<?php echo base_url('app/dashboard') ?>"><i class="icon-speedometer"></i><span>Dashboard</span></a>                                      
                                    </li>                                    
                                    <li class="header">แบบประเมิน</li>
                                    <li><a href=""><i class="icon-plus"></i><span>NEW</span></a></li>
                                    <li><a href=""><i class="icon-list"></i><span>PROB</span></a></li>
                                    <li><a href=""><i class="icon-list"></i><span>VITAL</span></a></li>
                                    <li><a href=""><i class="icon-list"></i><span>BODY</span></a></li>
                                    <li><a href="<?php echo base_url('app/ptot') ?>"><i class="icon-list"></i><span>PTOP</span></a></li>
                                    <li><a href=""><i class="icon-list"></i><span>FALL</span></a></li>
                                    <li><a href=""><i class="icon-list"></i><span>ADL</span></a></li>
                                    <li><a href=""><i class="icon-list"></i><span>COG</span></a></li>
                                    <li><a href=""><i class="icon-list"></i><span>HEENT</span></a></li>
                                    <li><a href=""><i class="icon-list"></i><span>UI</span></a></li>
                                    <li><a href=""><i class="icon-list"></i><span>SLEEP</span></a></li>
                                    <li><a href=""><i class="icon-list"></i><span>SLEEP2</span></a></li>
                                    <li><a href=""><i class="icon-list"></i><span>MNA</span></a></li>
                                    <li><a href=""><i class="icon-list"></i><span>CANC</span></a></li>
                                    <li><a href=""><i class="icon-list"></i><span>BONE</span></a></li>
                                    <li><a href=""><i class="icon-list"></i><span>LAB</span></a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="tab-pane" id="Profile-icon">
                            <nav class="sidebar-nav sidebar-scroll">
                                <div class="user-detail">
                                    <div class="text-center mb-3">
                                        <img class="img-thumbnail rounded-circle" src="../assets/images/sm/avatar1.jpg" alt="">
                                        <h6 class="mt-3 mb-0">

                                        </h6>
                                        <div class="text-center text-muted"></div>                                        
                                    </div>
                                    <hr>
                                    <div class="card-text">
                                        <div class="mt-0">
                                            <small class="float-right text-muted">100 GB</small>
                                            <span>Storage</span>
                                            <div class="progress progress-xxs">
                                                <div style="width: 60%;" class="progress-bar"></div>
                                            </div>
                                        </div>                                       
                                        <div class="mt-4">
                                            <small class="float-right text-muted">73%</small>
                                            <span>Booking</span>
                                            <div class="progress progress-xxs">
                                                <div style="width: 40%;" class="progress-bar"></div>
                                            </div>
                                        </div>
                                        <div class="mt-4">
                                            <small class="float-right text-muted">400 GB</small>
                                            <span>Member</span>
                                            <div class="progress progress-xxs mb-0">
                                                <div style="width: 80%;" class="progress-bar bg-danger"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </nav>
                        </div>
                        <div class="tab-pane" id="Envelope-icon">
                            <nav class="sidebar-nav sidebar-scroll">
                                <a href="app-compose.html" class="btn btn-danger btn-block mb-3">Compose</a>
                                <ul class="metismenu">
                                    <li class="active"><a href="app-inbox.html"><i class="icon-drawer"></i><span>Inbox</span><span class="badge badge-primary float-right">6</span></a></li>
                                    <li><a href="javascript:void(0);"><i class="icon-cursor"></i><span>Sent</span><span class="badge badge-warning float-right">6</span></a></li>
                                    <li><a href="javascript:void(0);"><i class="icon-envelope-open"></i><span>Draft</span></a></li>
                                    <li><a href="javascript:void(0);"><i class="icon-action-redo"></i><span>Outbox</span></a></li>
                                    <li><a href="javascript:void(0);"><i class="icon-trash"></i><span>Trash</span></a></li>
                                    <li class="header">Labels</li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-danger"></i><span>Workshop</span></a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-info"></i><span>Private</span></a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-dark"></i><span>Paypal</span></a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-primary"></i><span>Comapny</span></a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="tab-pane" id="Components-icon">
                            <nav class="sidebar-nav sidebar-scroll">
                                <ul class="metismenu">
                                    <li><a href="ui-helper-class.html"><i class="icon-info"></i>Helper Classes</a></li>
                                    <li><a href="ui-bootstrap.html"><i class="icon-key"></i>Bootstrap UI</a></li>
                                    <li><a href="ui-typography.html"><i class="icon-target"></i>Typography</a></li>
                                    <li><a href="ui-colors.html"><i class="icon-drop"></i>Colors</a></li>
                                    <li><a href="ui-buttons.html"><i class="icon-tag"></i>Buttons</a></li>
                                    <li><a href="ui-tabs.html"><i class="icon-tag"></i>Tabs</a></li>
                                    <li><a href="ui-progressbars.html"><i class="icon-graph"></i>Progress Bars</a></li>
                                    <li><a href="ui-modals.html"><i class="icon-tag"></i>Modals</a></li>
                                    <li><a href="ui-notifications.html"><i class="icon-question"></i>Notifications</a></li>
                                    <li><a href="ui-dialogs.html"><i class="icon-tag"></i>Dialogs</a></li>
                                    <li><a href="ui-list-group.html"><i class="icon-list"></i>List Group</a></li>
                                    <li><a href="ui-media-object.html"><i class="icon-tag"></i>Media Object</a></li>
                                    <li><a href="ui-nestable.html"><i class="icon-loop"></i>Nestable</a></li>
                                    <li><a href="ui-range-sliders.html"><i class="icon-equalizer"></i>Range Sliders</a></li>
                                    <li>
                                        <a href="#Charts" class="has-arrow"><i class="icon-bar-chart"></i><span>Charts</span></a>
                                        <ul>
                                            <li><a href="chart-c3.html">C3 Charts</a></li>
                                            <li><a href="chart-chartjs.html">ChartJS</a></li>
                                            <li><a href="chart-morris.html">Morris Charts</a></li>
                                            <li><a href="chart-flot.html">Flot Charts</a></li>
                                            <li><a href="chart-sparkline.html">Sparkline Chart</a></li>
                                            <li><a href="chart-jquery-knob.html">Jquery Knob</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#Forms" class="has-arrow"><i class="icon-layers"></i><span>Forms</span></a>
                                        <ul>
                                            <li><a href="forms-basic.html">Basic Elements</a></li>
                                            <li><a href="forms-advanced.html">Advanced Elements</a></li>
                                            <li><a href="forms-validation.html">Form Validation</a></li>
                                            <li><a href="forms-wizard.html">Form Wizard</a></li>
                                            <li><a href="forms-summernote.html">Summernote</a></li>
                                            <li><a href="forms-dragdropupload.html">Drag &amp; Drop Upload</a></li>                            
                                            <li><a href="forms-editors.html">CKEditor</a></li>
                                            <li><a href="forms-markdown.html">Markdown</a></li>
                                            <li><a href="forms-cropping.html">Image Cropping</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#Tables" class="has-arrow"><i class="icon-film"></i><span>Tables</span></a>
                                        <ul>
                                            <li><a href="table-normal.html">Normal Tables</a></li>
                                            <li><a href="table-color.html">Tables Color</a></li>
                                            <li><a href="table-jquery-datatable.html">Jquery Datatables</a></li>
                                            <li><a href="table-editable.html">Editable Tables</a></li>                            
                                            <li><a href="table-filter.html">Table Filter</a></li>
                                            <li><a href="table-dragger.html">Table dragger</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="tab-pane" id="Setting-icon">
                            <nav class="sidebar-nav sidebar-scroll">
                                <p>Choose Skin</p>
                                <ul class="choose-skin list-unstyled mb-0">
                                    <li data-theme="indigo" class="active"><div class="indigo"></div></li>
                                    <li data-theme="green"><div class="green"></div></li>
                                    <li data-theme="orange"><div class="orange"></div></li>
                                    <li data-theme="blush"><div class="blush"></div></li>
                                    <li data-theme="cyan"><div class="cyan"></div></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            <div id="main-content">
                <div class="block-header">
                    <div class="row clearfix">
                        <div class="col-md-6 col-sm-12">
                            <h2>แบบประเมินที่กำลังทำอยู่</h2>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#"><i class="fa fa-cube"></i></a></li>
                                    <li class="breadcrumb-item"><a href="#">Pages</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">รายการข้อมูล</li>
                                </ol>
                            </nav>
                        </div>            
                        <div class="col-md-6 col-sm-12 text-right hidden-xs">
                            <!--<a href="javascript:void(0);" class="btn btn-sm btn-primary btn-round" title="">เขียนคำร้อง</a>-->
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <!-- launch-pricing -->
                    <div class="modal fade launch-pricing-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">                                            
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body pricing_page text-center pt-4 mb-4">
                                    <h5>นักศึกษาท่านใดต้องการใช้ hosting for education ?</h5>
                                    <p class="mb-4">โปรดติดต่อที่ อ.ปรัชญานันท์ ญาณเพิ่ม โดยตรง ไม่มีค่าใช้จ่ายใดๆ</p>
                                    <div class="row clearfix">
                                        <div class="col-lg-4 cool-md-4 col-sm-12">
                                            <div class="card">
                                                <ul class="pricing body">
                                                    <li class="price">
                                                        <h3><span>$</span> 99 <small>/ mo</small></h3>
                                                        <span>Freelance</span>
                                                    </li>
                                                    <li>1 GB of space</li>
                                                    <li>Support at $25/hour</li>
                                                    <li>Limited cloud access</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 cool-md-4 col-sm-12">
                                            <div class="card">
                                                <ul class="pricing body active">
                                                    <li class="price">
                                                        <h3><span>$</span> 199 <small>/ mo</small></h3>
                                                        <span>Business</span>
                                                    </li>
                                                    <li>5 GB of space</li>
                                                    <li>Support at $10/hour</li>
                                                    <li>Full cloud access</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 cool-md-4 col-sm-12">
                                            <div class="card">
                                                <ul class="pricing body">
                                                    <li class="price">
                                                        <h3><span>$</span> 299 <small>/ mo</small></h3>
                                                        <span>Enterprise</span>
                                                    </li>
                                                    <li>15 GB of space</li>
                                                    <li>Support at $5/hour</li>
                                                    <li>Full cloud access</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <p>กรณีต้องการใช้เพื่อการพานิชย์ ใช่ไหม ? <a href="javascript:void(0);">ติดต่อ อ.ปรัชญานันท์ ญาณเพิ่ม</a> เพื่อส่งข้อมูลเพิ่มเติม.</p>
                                            <button class="btn btn-round btn-success">Start your free trial</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    