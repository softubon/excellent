<div class="col-lg-12">
    <div class="card">
        <div class="header">
            <h2>สมาชิกในระบบ แผนกเทคโนโลยีสารสนเทศ วิทยาลัยเทคนิคอุบลราชธานี<small>รายชื่อสมาชิกในระบบที่ทำการลงทะเบียน</small></h2>
            <ul class="header-dropdown dropdown">

                <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:void(0);">Action</a></li>
                        <li><a href="javascript:void(0);">Another Action</a></li>
                        <li><a href="javascript:void(0);">Something else</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive">
                <table class="table table-striped table-hover dataTable js-exportable">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รหัสนักศึกษา</th>
                            <th>ชื่อ สกุล</th>
                            <th>อีเมล์</th>
                            <th>แผนกสาขาวิชา</th>
                            <th>สถานะบัญชี</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รหัสนักศึกษา</th>
                            <th>ชื่อ สกุล</th>
                            <th>อีเมล์</th>
                            <th>แผนกสาขาวิชา</th>
                            <th>สถานะบัญชี</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        $i = 1;
                        $query = $this->db->get('tbstudent');
                        foreach ($query->result() as $row) {
                            ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $row->StudentID; ?></td>
                                <td><?php echo $row->StudentName; ?></td>
                                <td><?php echo $row->Email; ?></td>
                                <td><?php echo $row->Department; ?></td>
                                <td>
                                    <?php
                                    if ($row->ACTIVATE_STATUS == 1) {
                                        ?>
                                        <span class="badge badge-success">Active</span>
                                        <?php
                                    } else {
                                        ?>
                                        <span class="badge badge-danger">Waiting</span>
                                        <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>