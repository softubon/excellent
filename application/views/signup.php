<!doctype html>
<html lang="en">

<head>
<title>ICT UTC | Sign Up</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Qubes Bootstrap 4x admin is super flexible, powerful, clean &amp; modern responsive admin dashboard with unlimited possibilities.">
<meta name="author" content="GetBootstrap, design by: puffintheme.com">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/animate-css/vivify.min.css">

<!-- MAIN CSS -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/site.min.css">

</head>

<body class="theme-blue">

    <div class="auth-main">
        <div class="auth_div">
            <div class="card">
                <div class="auth_brand">
                    <a class="navbar-brand" href="javascript:void(0);"><i class="fa fa-cube font-25"></i> แผนกวิชาเทคโนโลยีสารสนเทศ</a>
                </div>
                <div class="body">
                    <p class="lead">สร้างบัญชีผู้ใช้งาน</p>
                    <form class="form-auth-small m-t-20" action="<?php echo base_url('app/create_account') ?>" method="POST">
                        <div class="form-group">
                            <input type="email" name="Username" class="form-control round" placeholder="Your email" required="">
                        </div>
                        <div class="form-group">                            
                            <input type="password" name="Password" class="form-control round" placeholder="Password" required="">
                        </div>
                        <div class="separator-linethrough"><span>ข้อมูลนักศึกษา</span></div>
                        <div class="form-group">
                            <input type="text" name="StudentID" class="form-control round" placeholder="รหัสนักศึกษา,เบอร์โทร,เลขบัตร 13 หลัก" required="">
                        </div>
                        <div class="form-group">
                            <input type="text" name="StudentName" class="form-control round" placeholder="ชื่อ-สกุล" required="">
                        </div>
                        <div class="form-group">
                            <input type="text" name="Department" class="form-control round" value="แผนกเทคโนโลยีสารสนเทศ" placeholder="แผนกวิชา" required="">
                        </div>
                        <div class="separator-linethrough"><span>อย่าลืมยืนยันอีเมล์ด้วยนะครับ</span></div>
                        <button type="submit" class="btn btn-primary btn-round btn-block">สมัครสมาชิกเลยจ้า</button>                                
                    </form>
                    
                </div>
            </div>
        </div>
        <div class="auth_right">
            <div id="slider2" class="carousel slide" data-ride="carousel" data-interval="3000">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                <div class="carousel-inner pb-5">
                    <div class="carousel-item active">
                        <img src="<?php echo base_url() ?>assets/images/login-slide1.png" class="img-fluid" alt="login page" />
                        <div class="px-4">
                            <h2>Cloud 4x</h2>
                            <p>เว็บโฮสติ้งค์เพื่อการศึกษา</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="<?php echo base_url() ?>assets/images/login-slide2.png" class="img-fluid" alt="login page" />
                        <div class="px-4">
                            <h2>100% Secure</h2>
                            <p>ข้อมูลมีความปลอดภัยสูง</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="<?php echo base_url() ?>assets/images/login-slide3.png" class="img-fluid" alt="login page" />
                        <div class="px-4">
                            <h2>Saving Time</h2>
                            <p>ประหยัดเวลาการเรียนรู้</p>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>

    <!-- END WRAPPER -->

<!-- Latest jQuery -->
<script src="<?php echo base_url() ?>assets/vendor/jquery/jquery-3.3.1.min.js"></script>
<!-- Bootstrap 4x JS  -->
<script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="<?php echo base_url() ?>assets/bundles/vendorscripts.bundle.js"></script>
<script src="<?php echo base_url() ?>assets/js/common.js"></script>
</body>
</html>